import jaydebeapi
from django.http import HttpResponse
import json
import os

class JDBCConnection:
    driver = 'org.apache.derby.jdbc.ClientDriver'
    dbname = 'db_jdbc'
    connectionURL = 'jdbc:derby://127.0.0.1:1527/%s' % dbname
    
    
    def set_environ(self):
        if not 'DERBY_HOME' in os.environ.keys():
            os.environ['DERBY_HOME'] = '/opt/derby'
        if not 'JAVA_HOME' in os.environ.keys():
            os.environ['JAVA_HOME'] = '/usr/java/jdk1.8.0'
    
    def connect(self):
        self.set_environ()
        jar = os.path.join(
            os.environ.get('DERBY_HOME'),
            'lib',
            'derbyclient.jar')
        
        return jaydebeapi.connect(
            jclassname=self.driver, 
            driver_args=[self.connectionURL, 'teste', 'Teste123'], 
            jars=jar)
    
    def query(self, query):
        result = []
        try:
            con = self.connect()
            cursor = con.cursor()
            cursor.execute(query)
            result = cursor.fetchall()
            
        except Exception as e:
            raise e
        cursor.close()
        con.close()  
        
        return result
        
    
def get_cadastrados(request):
    db = JDBCConnection()
    result = db.query('SELECT * FROM TESTE2')
    
    return HttpResponse(json.dumps(result), mimetype='application/javascript')